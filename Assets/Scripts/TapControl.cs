﻿using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;


/// <summary>
/// This check if receive a short tap, a long tap or a double tap. 
/// Has a delegate for each situation.
/// </summary>
public class TapControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {

	public System.Action<TapControl> doubleTapDelegate;
	public System.Action<TapControl> shortTapDelegate;
	public System.Action<TapControl> longTapDelegate;

	private static float doublePressInterval = 0.25f;
	private static float longPressTime = 0.75f;

	private bool interactive = false;
	private float lastTime;
	private bool pressDown;
	private float time;

	void Update() {
		time += Time.deltaTime;

		if (interactive && time > longPressTime && pressDown) {
			if (longTapDelegate != null) {
				longTapDelegate(this);
			}
			interactive = false;
		}

		if (interactive && time > doublePressInterval && !pressDown) {
			if (shortTapDelegate != null) {
				shortTapDelegate(this);
			}
			interactive = false;
		}
	}

	public void OnPointerDown (PointerEventData eventData) {

		if (interactive && time < doublePressInterval) {
			if (doubleTapDelegate != null) {
				doubleTapDelegate(this);
			}
			interactive = false;
			time = 0;
			return;
		}

		interactive = true;

		time = 0;
		pressDown = true;
	}

	public void OnPointerUp (PointerEventData eventData) {
		time = 0;
		pressDown = false;
	}

	public void OnPointerExit (PointerEventData eventData) {
		interactive = false;
		time = 0;
		pressDown = false;
	}

}
