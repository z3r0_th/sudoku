﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CellControl : MonoBehaviour {

	public System.Action<CellControl> numberUpdate;

	public RectTransform numberPanel;
	public RectTransform hintPanel;
	public InputHandler input;


	private Text numberText;
	private int number;

	public int Number {
		get {
			return number;
		}
	}


	void Awake () {
		numberText = numberPanel.GetComponentInChildren<Text>();
		TapControl tapControl = null;
		foreach(Transform hintTransform in hintPanel.transform) {
			Color c = hintTransform.GetComponent<Image>().color;
			c.a = activeAlphaColor;
			hintTransform.GetComponent<Image>().color = c;

			tapControl = hintTransform.gameObject.AddComponent<TapControl>();
			tapControl.doubleTapDelegate = DoubleTapDelegate;
			tapControl.shortTapDelegate = ShortTapDelegate;
			tapControl.longTapDelegate = LongTapDelegate;
		}

		tapControl = numberPanel.transform.gameObject.AddComponent<TapControl>();
		tapControl.doubleTapDelegate = NumberShortTapDelegate;
		tapControl.shortTapDelegate = NumberShortTapDelegate;
		tapControl.longTapDelegate = NumberLongTapDelegate;

		numberPanel.gameObject.SetActive(false);
		hintPanel.gameObject.SetActive(true);
		input.gameObject.SetActive(false);

		input.InputSubmitStringDelegate = InputSubmit;
		input.InputField.characterValidation = InputField.CharacterValidation.Integer;
	}

	void InputSubmit(string submitString) {
		if (submitString == "" || submitString == " ") submitString = "1";
		numberText.text = submitString;
		input.gameObject.SetActive(false);
		number = int.Parse(submitString);
		if (numberUpdate != null) {
			numberUpdate(this);
		}
	}

	void NumberShortTapDelegate(TapControl control) {
		input.gameObject.SetActive(true);
		input.InputField.ActivateInputField();
		input.InputField.Select();
	}

	void NumberLongTapDelegate(TapControl control) {
		EnableNumber(false);
	}

	float activeAlphaColor = 0.7f;
	void ShortTapDelegate(TapControl control) {
		Color c = control.GetComponent<Image>().color;
		c.a = c.a == activeAlphaColor ? 0.35f : activeAlphaColor;
		control.GetComponent<Image>().color = c;
		control.GetComponentInChildren<Text>().color = new Color(0,0,0,c.a);
	}

	void LongTapDelegate(TapControl control) {
		EnableNumber(true);
	}

	void DoubleTapDelegate(TapControl control) {
		EnableNumber(true);
	}

	void EnableNumber(bool enable) {
		numberPanel.gameObject.SetActive(enable);
		hintPanel.gameObject.SetActive(!enable);
		input.gameObject.SetActive(enable);
		input.InputField.ActivateInputField();
		input.InputField.Select();
		if (!enable) {
			number = -1;
			if (numberUpdate != null) {
				numberUpdate(this);
			}
		}
	}
}
