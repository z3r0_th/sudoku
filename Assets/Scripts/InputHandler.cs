﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class InputHandler : MonoBehaviour, ISubmitHandler {

	public System.Action<string> InputSubmitStringDelegate;
	public System.Action<InputField> InputSubmitDelegate;

	protected InputField inputField;

	private bool lastSelected = false;


	public InputField InputField {
		get {
			return this.inputField;
		}
	}

	void Awake() {
		inputField = GetComponent<InputField> ();
	}

	public void OnSubmit (BaseEventData eventData) {
		if (InputSubmitDelegate != null) {
			InputSubmitDelegate (inputField);
		}

		if (InputSubmitStringDelegate != null) {
			InputSubmitStringDelegate (inputField.text);
		}

	}

	void Update() {
		if ((Input.GetKeyDown (KeyCode.Return) || Input.GetKeyDown (KeyCode.KeypadEnter)) && lastSelected) {
			InputSubmitStringDelegate (inputField.text);
		}

		lastSelected = inputField.isFocused;
	}
}